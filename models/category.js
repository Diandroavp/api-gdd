var sequelize = require('../config/database').sequelize_con;

function getCategory( callback){
    console.log("function:", "getCategory");

    sequelize.query(`   SELECT  *
                        FROM    category`,
    {replacements: {},
    type: sequelize.QueryTypes.SELECT}).then(function(result){
        callback( result.length > 0 ? result : null);        
    });
}

function getCategoryId(id, callback){
    console.log("function:", "getCategoryId");

    sequelize.query(`   SELECT  *
                        FROM    category
                        WHERE   id  =   :id`,
    {replacements: {id:id},
    type: sequelize.QueryTypes.SELECT}).then(function(result){
        callback( result.length > 0 ? result : null);        
    });
}


function insertCategory(name, callback){
    console.log("function:", "insertCategory");
    sequelize.query(`  INSERT INTO category (name) VALUES (:name)`,
    {replacements: {name:name},
    type: sequelize.QueryTypes.INSERT}).then(function(result){
        callback( true);        
    });
}

function deleteCategory(id, callback){
    console.log("function:", "deleteCategory");
    sequelize.query(`  DELETE FROM category
                       WHERE id = :id`,
    {replacements: {id:id},
    type: sequelize.QueryTypes.DELETE}).then(function(result){
        callback( true);        
    });
}

function updateCategory(id, name, callback){
    console.log("function:", "updateCategory");
    sequelize.query(`   UPDATE category
                        SET name = :name
                        WHERE id = :id`,
    {replacements: {id:id, name:name},
    type: sequelize.QueryTypes.UPDATE}).then(function(result){
        callback( true);        
    });
}

module.exports.getCategory = getCategory;
module.exports.insertCategory = insertCategory;
module.exports.deleteCategory = deleteCategory;
module.exports.updateCategory = updateCategory;
module.exports.getCategoryId = getCategoryId;
