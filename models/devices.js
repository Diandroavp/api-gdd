var sequelize = require('../config/database').sequelize_con;

function getDevices( callback){
    console.log("function:", "getDevices");

    sequelize.query(`   SELECT  devices.*, category.name
                        FROM    devices
                        LEFT JOIN category ON  devices.idCategory = category.id
                        `,
    {replacements: {},
    type: sequelize.QueryTypes.SELECT}).then(function(result){
        callback( result.length > 0 ? result : null);        
    });
}

function getDevicesId(id, callback){
    console.log("function:", "getDevicesId");

    sequelize.query(`   SELECT  *
                        FROM    devices
                        WHERE   id  =   :id`,
    {replacements: {id:id},
    type: sequelize.QueryTypes.SELECT}).then(function(result){
        callback( result.length > 0 ? result : null);        
    });
}

function insertDevices(idCategory, color, partNumber, callback){
    console.log("function:", "insertDevices");
    sequelize.query(`  INSERT INTO devices (idCategory, color, partNumber) VALUES (:idCategory, :color, :partNumber)`,
    {replacements: {idCategory:idCategory, color:color, partNumber:partNumber},
    type: sequelize.QueryTypes.INSERT}).then(function(result){
        callback( true);        
    });
}

function deleteDevices(id, callback){
    console.log("function:", "deleteDevices");
    sequelize.query(`  DELETE FROM devices
                       WHERE id = :id`,
    {replacements: {id:id},
    type: sequelize.QueryTypes.DELETE}).then(function(result){
        callback( true);        
    });
}

function updateDevices(id, idCategory, color, partNumber, callback){
    console.log("function:", "updateDevices");
    sequelize.query(`   UPDATE devices
                        SET idCategory  =   :idCategory, 
                            color       =   :color, 
                            partNumber  =   :partNumber
                        WHERE id = :id`,
    {replacements: {id:id, idCategory:idCategory, color:color, partNumber:partNumber},
    type: sequelize.QueryTypes.UPDATE}).then(function(result){
        callback( true);        
    });
}

module.exports.getDevices = getDevices;
module.exports.insertDevices = insertDevices;
module.exports.deleteDevices = deleteDevices;
module.exports.updateDevices = updateDevices;
module.exports.getDevicesId = getDevicesId;
