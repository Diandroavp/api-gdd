 var category = require('../models/category');
 var devices = require('../models/devices');

 /********************************Category *******************************************/
 module.exports.listCategory = function (request, callback) {
    category.getCategory( function(obj_category) {
        if (obj_category != null) {
            var data = {
                code: 200,
                mensagem: "Lista de categorias!",
                data: obj_category
            }
            callback(data);
        } else {
            var data = {
                code: 400,
                mensagem: "Não possui dados!",
                data: ''
            }
            callback(data);
        }
        
    });
}

module.exports.insertCategory = function (request, callback) {
    var name = request.body.name;    
    if (name != null) {
        category.insertCategory(name, function(obj_category) {            
                var data = {
                    code: 200,
                    mensagem: "Inserido com sucesso"
                }
                callback(data);
        });
    } else {
        var data = {
            code: 400,
            mensagem: "Parâmetros não passado corretamente!"
        }
        callback(data);
    }
}

module.exports.deleteCategory = function (request, callback) {
    var id = request.body.id;    
    if (id != null) {
        category.deleteCategory(id, function(obj_category) {            
                var data = {
                    code: 200,
                    mensagem: "Deletado com sucesso"
                }
                callback(data);
        });
    } else {
        var data = {
            code: 400,
            mensagem: "Parâmetros não passado corretamente!"
        }
        callback(data);
    }    
}


module.exports.updateCategory = function (request, callback) {
    var id = request.body.id;    
    var name = request.body.name;    
    if (id != null) {
        category.updateCategory(id,name,  function(obj_category) {            
                var data = {
                    code: 200,
                    mensagem: "Atualizado com sucesso"
                }
                callback(data);
        });
    } else {
        var data = {
            code: 400,
            mensagem: "Parâmetros não passado corretamente!"
        }
        callback(data);
    }    
}

module.exports.getCategoryId = function (request, callback) {
    var id = request.body.id;        
    if (id != null) {
        category.getCategoryId(id,  function(obj_category) {            
                var data = {
                    code: 200,
                    mensagem: "Atualizado com sucesso",
                    data: obj_category
                }
                callback(data);
        });
    } else {
        var data = {
            code: 400,
            mensagem: "Parâmetros não passado corretamente!",
            data: []
        }
        callback(data);
    }    
}


/********************************Devices *******************************************/

module.exports.listDevices = function (request, callback) {
    devices.getDevices( function(obj_devices) {
        if (obj_devices != null) {
            var data = {
                code: 200,
                mensagem: "Lista de dispositivos!",
                data: obj_devices
            }
            callback(data);
        } else {
            var data = {
                code: 400,
                mensagem: "Não possui dados!",
                data: ''
            }
            callback(data);
        }
        
    });
}

module.exports.insertDevices = function (request, callback) {
    var idCategory = request.body.idCategory;  
    var color = request.body.color;  
    var partNumber = request.body.partNumber;  
    
    if (idCategory != null) {
        devices.insertDevices(idCategory, color, partNumber, function(obj_devices) {            
                var data = {
                    code: 200,
                    mensagem: "Inserido com sucesso"
                }
                callback(data);
        });
    } else {
        var data = {
            code: 400,
            mensagem: "Parâmetros não passado corretamente!"
        }
        callback(data);
    }
}

module.exports.deleteDevices = function (request, callback) {
    var id = request.body.id;    
    if (id != null) {
        devices.deleteDevices(id, function(obj_devices) {            
                var data = {
                    code: 200,
                    mensagem: "Deletado com sucesso"
                }
                callback(data);
        });
    } else {
        var data = {
            code: 400,
            mensagem: "Parâmetros não passado corretamente!"
        }
        callback(data);
    }    
}

module.exports.updateDevices = function (request, callback) {
    var id = request.body.id;    
    var idCategory = request.body.idCategory;  
    var color = request.body.color;  
    var partNumber = request.body.partNumber;  

    if (id != null) {
        devices.updateDevices(id,idCategory, color, partNumber,  function(obj_devices) {            
                var data = {
                    code: 200,
                    mensagem: "Atualizado com sucesso"
                }
                callback(data);
        });
    } else {
        var data = {
            code: 400,
            mensagem: "Parâmetros não passado corretamente!"
        }
        callback(data);
    }    
}

module.exports.getDevicesId = function (request, callback) {
    var id = request.body.id;        
    if (id != null) {
        devices.getDevicesId(id,  function(obj_devices) {            
                var data = {
                    code: 200,
                    mensagem: "Atualizado com sucesso",
                    data: obj_devices
                }
                callback(data);
        });
    } else {
        var data = {
            code: 400,
            mensagem: "Parâmetros não passado corretamente!",
            data: []
        }
        callback(data);
    }    
}