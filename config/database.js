const Sequelize = require('sequelize');

// GCP

const sequelize = new Sequelize('dbGds', 'aluno', '12345678', {
  dialect: 'mysql',
  host: '/cloudsql/teste-diandro:us-central1:estudo',
  timestamps: false,
  dialectOptions: {
    socketPath: '/cloudsql/teste-diandro:us-central1:estudo',
    useUTC: true,
  },
  timezone: 'America/Manaus'
});

// TESTE LOCAL
/*
const DB = 'mysql://aluno:12345678@35.226.195.59:3306/dbGds';

const sequelize = new Sequelize(DB, {
  // operatorsAliases: false,
  dialectOptions: {
      useUTC: false, //for reading from database
          dateStrings: true,
          typeCast: true
  },
  pool: {
      max: 7,
      min: 2,
      acquire: 30000,
      idle: 10000,
    },
    timezone: 'America/Manaus' //for writing to database
    //logging: false
});
*/
module.exports.sequelize_con = sequelize;
