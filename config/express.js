const express    = require('express');
//const config     = require('config');

module.exports = () => {
    const app = express();

    // SETANDO VARIÁVEIS DA APLICAÇÃO
    app.set('port', process.env.PORT || 8080);
    
   require('../routes/app')(app);  
    
    // MIDDLEWARES
  
  return app;
};