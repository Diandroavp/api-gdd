var ApiController = require('../controllers/apiController');
var bodyParser = require('body-parser');
var cors = require('cors')


module.exports = app => {
    app.use(bodyParser.json());
    app.use(cors());

    // middleware that is specific to this router
    app.use(function timeLog(req, res, next) {
        console.log('Time: ', Date.now());
        //console.log(req.body)
        next();
    });

    app.route("/").get((req, res) => {
        res.send("API ATIVA");
    });

    //ROTAS
    /********************************Categorias *******************************************/
    app.route('/app/list_category').get((req, res) => {
        ApiController.listCategory(req, function (resposta) {
            res.send(resposta);
        });

    });

    app.route('/app/insert_category').post((req, res) => {
        ApiController.insertCategory(req, function (resposta) {
            res.send(resposta);
        });

    });

    app.route('/app/delete_category').post((req, res) => {
        ApiController.deleteCategory(req, function (resposta) {
            res.send(resposta);
        });

    });

    app.route('/app/update_category').post((req, res) => {
        ApiController.updateCategory(req, function (resposta) {
            res.send(resposta);
        });

    });

    app.route('/app/category_id').post((req, res) => {
        ApiController.getCategoryId(req, function (resposta) {
            res.send(resposta);
        });

    });

    /********************************Devices *******************************************/

    app.route('/app/list_device').get((req, res) => {
        ApiController.listDevices(req, function (resposta) {
            res.send(resposta);
        });

    });

    app.route('/app/insert_device').post((req, res) => {
        ApiController.insertDevices(req, function (resposta) {
            res.send(resposta);
        });

    });

    app.route('/app/delete_device').post((req, res) => {
        ApiController.deleteDevices(req, function (resposta) {
            res.send(resposta);
        });

    });

    app.route('/app/update_device').post((req, res) => {
        ApiController.updateDevices(req, function (resposta) {
            res.send(resposta);
        });

    });

    app.route('/app/device_id').post((req, res) => {
        ApiController.getDevicesId(req, function (resposta) {
            res.send(resposta);
        });

    });


  }
